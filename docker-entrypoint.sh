#!/bin/bash

set -euo pipefail


# On first start, /var/lib/lemonldap-ng will be empty, so we need to restore the files.
if [ -z "$(ls -A /var/lib/lemonldap-ng)" ]
then
    cp -r /var/lib/lemonldap-ng.save/* /var/lib/lemonldap-ng
fi


/usr/sbin/llng-fastcgi-server --foreground