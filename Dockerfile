FROM perl:5-slim

ENV LEMONLDAP_VERSION 2.16.1
ENV FLAP_VERSION 1
ENV VITAL_VERSION v2.2.1

COPY rpm-gpg-key-ow2 rpm-gpg-key-ow2

RUN apt update && \
    # apt update && \
    apt install -y \
    gcc \
    gnupg-utils \
    libdigest-hmac-perl \
    libexpat1-dev \
    libglib-perl \
    libglib2.0-dev \
    liblasso-perl \
    libssl-dev \
    pkg-config \
    wget
# Download lemon's deb packages.
# HACK: don't install from debian's repo because they are not up to date.
# HACK: don't install from lemonLDAP's repo because they don't support armhf arch.
# HACK: so we download deb packages from lemonLDAP's website and install them with dpkg.
RUN wget "https://release.ow2.org/lemonldap/lemonldap-ng-${LEMONLDAP_VERSION}_deb.tar.gz"
# Add release key.
RUN cat rpm-gpg-key-ow2 | apt-key add -
RUN gpg --import rpm-gpg-key-ow2
RUN gpg --allow-multiple-messages --verify lemonldap-ng-${LEMONLDAP_VERSION}_deb.tar.gz
RUN mkdir packages
RUN gpgtar --directory packages --extract lemonldap-ng-${LEMONLDAP_VERSION}_deb.tar.gz
# Install lemonLDAP packages.
RUN apt install -y ./packages/*.deb && \
    # Get Vital css framework to custom portal.
    wget https://cdn.rawgit.com/doximity/vital/$VITAL_VERSION/dist/css/vital.min.css --output-document=/usr/share/lemonldap-ng/portal/htdocs/static/bwr/vital.min.css && \
    # Reduce final image size.
    apt remove -y \
    wget \
    gcc \
    gnupg-utils \
    libssl-dev \
    libglib2.0-dev \
    libexpat1-dev \
    nginx-extras && \
    apt autoremove -y && \
    rm -rf packages && \
    rm -rf lemonldap-ng-${LEMONLDAP_VERSION}_deb.tar.gz && \
    rm -rf lemonldap-ng-${LEMONLDAP_VERSION}_deb.tar.gz.sha256 && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    # Move lib's files, they will be restored in docker-entrypoint.sh.
    mv /var/lib/lemonldap-ng /var/lib/lemonldap-ng.save && \
    # Create conf directory.
    mkdir --parent /etc/lemonldap-ng/conf && \
    chown www-data:www-data /etc/lemonldap-ng/conf

VOLUME ["/etc/lemonldap-ng/conf", "/var/lib/lemonldap-ng"]

# Change runtime user.
USER www-data

# Prevent startup error because default is /root.
WORKDIR /

ENV LISTEN 0.0.0.0:9000

# It might be usefull to reduce the process number to reduce ressource usage of lemonldap.
# TODO: check if this have an impact.
# ENV NPROC 3

COPY ./docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
